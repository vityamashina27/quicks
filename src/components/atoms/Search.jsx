import Image from "next/image";

export default function Search(params) {
	return (
		<div className="h-[58px] w-full bg-[#4F4F4F] flex">
			<Image
				src="/search.svg"
				width={16}
				height={16}
				className="my-auto ml-[26px]"
				alt={''}
			/>
		</div>
	);
}
