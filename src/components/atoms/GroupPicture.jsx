import Image from "next/image";

export default function GroupPicture(params) {
	return (
		<div className="relative mr-[17px]">
			<div className="bg-blue1 w-[34px] h-[34px] rounded-full flex justify-center items-center absolute top-0 left-[17px]">
				<Image src="/person-white.svg" width={18} height={18} />
			</div>
			<div className="bg-[#E0E0E0] w-[34px] h-[34px] rounded-full flex justify-center items-center">
				<Image src="/person-white.svg" width={18} height={18} />
			</div>
		</div>
	);
}
