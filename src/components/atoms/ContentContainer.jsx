export default function ContentContainer({ children , className }) {
	return (
		<div className={`absolute end-[34px] bottom-[110px] w-[734px] h-[737px] bg-white rounded-md py-[20px] px-[34px] ${className}`}>
			{children}
		</div>
	);
}
