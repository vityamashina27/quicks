import Image from "next/image";

export default function SearchBox(params) {
    return (
        <div className="border-[1px] px-[58.82px] py-2.5 border-gray1 rounded-md flex">
            <input placeholder="Search" className="w-full"/>
            <Image src={'/search-black.svg'} width={12} height={12} alt="" className="text-emerald-400"/>
        </div>
    )
};
