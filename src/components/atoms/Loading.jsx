export default function Loading({title}) {
    return (
        <div className="flex flex-col items-center">
            <div class="spinner"></div>
            {title}
        </div>
    )
};
