import Image from "next/image";

export default function ButtonMenuItem({ onClick, title, image, width, height }) {
	return (
		<div className="flex flex-col items-center gap-1 pb-[0.3em]">
			<span className="text-white">{title}</span>
			<button onClick={onClick} className="bg-white rounded-full w-[60px] h-[60px] flex justify-center align-center">
				<Image src={image} width={width} height={height} alt={title} />
			</button>
		</div>
	);
}
