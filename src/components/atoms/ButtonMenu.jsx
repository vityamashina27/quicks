"use client"
import Image from "next/image";
import { useRouter } from "next/navigation";

export default function ButtonMenu({
	onClick,
	image,
	width,
	height,
	bgColor = 'bg-[#2F80ED]',
	isActive,
}) {
	const router = useRouter()
	return (
		<div className="mt-auto relative">
			<button
				onClick={onClick}
				className={`${bgColor} rounded-full w-[68px] h-[68px] flex justify-center align-center ${
					isActive ? "absolute end-0" : ""
				}`}
			>
				<Image src={image} width={width} height={height} alt={""} />
			</button>
			{isActive && (
				<div onClick={()=> router.push('/') } className="bg-[#4F4F4F] rounded-full w-[68px] h-[68px] flex justify-center align-center mr-5 cursor-pointer"></div>
			)}
		</div>
	);
}
