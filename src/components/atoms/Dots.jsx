export default function Dots(params) {
	return (
		<div className="flex justify-center items-end ml-auto">
			<div className="w-[10px] h-[10px] rounded-full bg-red-500"></div>
		</div>
	);
}
