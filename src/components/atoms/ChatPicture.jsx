import Image from "next/image";
export default function ChatPicture(params) {
	return (
		<div className="mx-[8.5px]">
			<div className="bg-blue1 w-[34px] h-[34px] rounded-full flex justify-center items-center">
				<Image src="/person-white.svg" width={18} height={18} />
			</div>
		</div>
	);
}
