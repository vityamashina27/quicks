import Loading from "../atoms/Loading";

export default function CenterLoading(params) {
	return (
		<div className="relative">
			<div className="absolute top-96 left-96">
				<Loading title={"Loading Chats ..."} />
			</div>
		</div>
	);
}
