"use client";
import { useEffect, useState } from "react";
import ButtonMenu from "../atoms/ButtonMenu";
import ButtonMenuItem from "../atoms/ButtonMenuItem";
import { useRouter, useSearchParams } from "next/navigation";

export default function FloatingButton(params) {
	const searchParams = useSearchParams();
	const type = searchParams.get("type");

	const router = useRouter();
	const [show, setShow] = useState(false);
	const [menu, setMenu] = useState("");

	const handleShowMenu = () => {
		setShow((prev) => !prev);
	};

	const handleMenu = (menu) => {
		setMenu(menu);
		menu ? router.push(`/${menu}?type=${menu}`) : router.push("/");
	};

	useEffect(() => {
		!show && setMenu("");
	}, [show]);

	const taskButton = (title) => {
		return (
			<ButtonMenuItem
				onClick={() => handleMenu("task")}
				image={"/task.svg"}
				width={26}
				height={26}
				title={title}
			/>
		);
	};
	const inboxButton = (title) => {
		return (
			<ButtonMenuItem
				onClick={() => handleMenu("inbox")}
				image={"/chats.svg"}
				width={26}
				height={26}
				title={title}
			/>
		);
	};

	const doItemWhenTypeExists = (type) => {
		return type == "inbox" ? taskButton(null) : inboxButton(null);
	};

	const doMainWhenTypeExists = (type) => {
		return type == "inbox" ? (
			<ButtonMenu
				onClick={() => handleMenu('inbox')}
				image={"/chats-white.svg"}
				width={30}
				height={30}
				bgColor="bg-[#8785FF]"
				isActive={type ? true : false}
			/>
		) : 
		<ButtonMenu
				onClick={() => handleMenu('task')}
				image={"/task-white.svg"}
				width={30}
				height={30}
				bgColor="bg-[#F8B76B]"
				isActive={type ? true : false}
			/>;
	};

	return (
		<div className="absolute end-[34px] bottom-[27px] flex gap-4 items-end">
			{type
				? doItemWhenTypeExists(type)
				: show && (
						<>
							{taskButton(!menu && "Task")}
							{inboxButton(!menu && "Inbox")}
						</>
				)}

			{type ? doMainWhenTypeExists(type) : 
			<ButtonMenu
				onClick={handleShowMenu}
				image={"/thunder.svg"}
				width={56}
				height={56}
				isActive={type ? true : false}
			/>
			}
		</div>
	);
}
