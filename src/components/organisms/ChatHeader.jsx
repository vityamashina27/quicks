"use client";
import Image from "next/image";
import { useRouter } from "next/navigation";

export default function ChatHeader(params) {
	const router = useRouter();

	return (
		<div className="flex gap-4 pb-5 border-b-[1px] px-9 ">
			<Image
				src={"/back.svg"}
				width={24}
				height={24}
				onClick={() => router.push("/inbox?type=inbox")}
				className="cursor-pointer"
			/>
			<div className="flex flex-col gap-1">
				<span className="font-normal text-blue1 text-lg max-w-[414px]">
					I-589 - AMARKHIL, Obaidullah [Affirmative Filing with ZHN]
				</span>
				<span>3 Participants</span>
			</div>
			<Image
				src={"/close.svg"}
				width={14}
				height={14}
				className="ms-auto cursor-pointer"
                onClick={()=>router.push('/')}
			/>
		</div>
	);
}
