import Image from "next/image";
import Link from "next/link";
import GroupPicture from "../atoms/GroupPicture";
import Dots from "../atoms/Dots";
import ChatPicture from "../atoms/ChatPicture";

export default function ChatItem({
	showBorder,
	participants,
	sender,
	message,
	date,
	isRead = true,
	isGroup = false,
}) {
	return (
		<Link href="/inbox/conversation?type=inbox"
			className={`flex justify-start pt-[23.5px] gap-[17px] mb-[39px] cursor-pointer ${
				showBorder ? "border-t-[1px] border-gray3" : ""
			}`}
		>
		
			{isGroup ? <GroupPicture/> : <ChatPicture/>}
			
			<div className="flex flex-col">
				<div className="flex gap-[17px] items-start">
					<span className="mb-2 font-normal text-blue1 text-lg max-w-[414px]">
						{participants}
					</span>
					<span>{date}</span>
				</div>
				{sender && <span className="font-normal">{sender} :</span>}
				<span className="font-light">{message}</span>
			</div>
			
			{!isRead && <Dots/>}
		</Link>
	);
}
