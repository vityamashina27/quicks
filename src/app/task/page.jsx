import ContentContainer from "@/components/atoms/ContentContainer";
import SearchBox from "@/components/atoms/SearchBox";
import CenterLoading from "@/components/molecules/CenterLoading";

export default function page(params) {
    return (
        <ContentContainer>
            <SearchBox/>
            <CenterLoading/>
        </ContentContainer>
    )
};
