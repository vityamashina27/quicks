"use client";
import ContentContainer from "@/components/atoms/ContentContainer";
import SearchBox from "@/components/atoms/SearchBox";
import CenterLoading from "@/components/molecules/CenterLoading";
import ChatHeader from "@/components/organisms/ChatHeader";
import Image from "next/image";
import { useEffect, useState } from "react";

export default function page(params) {
	const [loading, setLoading] = useState(true);
	useEffect(() => {
		setTimeout(() => {
			setLoading(false);
		}, 1500);
	}, []);

	const chats = () => {
		return (
			<div className="p-1 flex flex-col gap-3">
				<div className="flex items-center gap-4 justify-center">
					<span className="w-2/5 h-[1px] border-[1px]"></span>
					<span className="text-xl font-normal">Today June 09, 2021</span>
					<span className="w-2/5 h-[1px] border-[1px]"></span>
				</div>
				<div className="flex flex-col gap-2 items-end">
					<span className="font-semibold text-my-bubble-dark">
						You
					</span>
					<div className="flex gap-2 justify-end items-start">
						<Image src={"/dots.svg"} width={16} height={16} className="cursor-pointer" />
						<div className="w-3/4 bg-my-bubble p-3 rounded-md text-base">
							Please contact Mary for questions regarding the case
							bcs she will be managing your forms from now on!
							Thanks Mary.
						</div>
					</div>
				</div>
			</div>
		);
	};

	return (
		<ContentContainer className={"px-[0px] py-3"}>
			<ChatHeader />
			<div className="m-5">{loading ? <CenterLoading /> : chats()}</div>
		</ContentContainer>
	);
}
