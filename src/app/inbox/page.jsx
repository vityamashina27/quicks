"use client";
import ContentContainer from "@/components/atoms/ContentContainer";
import SearchBox from "@/components/atoms/SearchBox";
import CenterLoading from "@/components/molecules/CenterLoading";
import ChatItem from "@/components/organisms/ChatItem";
import { useEffect, useState } from "react";

export default function page(params) {
	const [loading, setLoading] = useState(true);
	useEffect(() => {
		setTimeout(() => {
			setLoading(false);
		}, 1500);
	}, []);

	const chats = () => {
		return (
			<>
				<ChatItem
					participants={"109220-Naturalization"}
					sender={"Cameron Philips"}
					message={"Please check this out!"}
					date={"January, 1, 2021 19:10"}
					isRead={false}
					isGroup={true}
				/>
				<ChatItem
					showBorder={true}
					participants={
						"Jeannette Moraima Guaman Chamba (Hutto I-589) [ Hutto Follow Up - Brief Service ]"
					}
					sender={"Ellen"}
					message={"Hey, please read."}
					date={"02/06/2021 10:45"}
					isGroup={true}
				/>
				<ChatItem
					showBorder={true}
					participants={"8405-Diana SALAZAR MUNGUIA"}
					sender={"Cameron Phillips"}
					message={
						"I understand your initial concerns and thats very valid, Elizabeth. But you ..."
					}
					date={"01/06/2021 12:19"}
					isGroup={true}
				/>
				<ChatItem
					showBorder={true}
					participants={"FastVisa Support"}
					message={"Hey there! Welcome to your inbox."}
					date={"January, 1, 2021 19:10"}
				/>
			</>
		);
	};

	return (
		<ContentContainer>
			<SearchBox />
			{loading ? <CenterLoading /> : chats()}
		</ContentContainer>
	);
}
